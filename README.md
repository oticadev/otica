# Otica

Otica是Open-source Transport Intelligence Card Analyzer的缩写，基于[GNU GPLv3](./LICENSE)的开源项目，旨在开发开源的交通联合卡、城市联合卡以及地铁票的分析软件。